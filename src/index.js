import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dashboard from './pages/dashboard/Index'
import AddData from './pages/dashboard/addData'
import Detail from './pages/dashboard/detail'

ReactDOM.render(
  <React.StrictMode>
    <Router>
     <Switch>
          {/* <Route exact path="/" component={App} /> */}
          <Route exact path="/" component={Dashboard} />
          <Route exact path="/addData" component={AddData} />
          <Route exact path="/detail/:id" component={Detail} />
          <Route exact path="/detail" component={Detail} />
        </Switch>
      </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
