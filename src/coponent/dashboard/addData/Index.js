import axios from "axios";
import React, { useState } from "react";
import * as yup from "yup";

function Index() {
  const SUPPORTED_FORMATS = ["image/jpg", "image/jpeg", "image/png"];
  const [coba, setcoba] = useState({});
  const [body, setBody] = useState("");
  const [image, setImage] = useState("");
  const [content, setcontent] = useState({
    title: "",
    body: 0,
    hargabeli: 0,
    hargajual: 0,
    conditional: "",
  });

  const handleChange = (e) => {
    e.persist();
    setcontent((content) => ({
      ...content,
      [e.target.name]: e.target.value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    let data = yup.object().shape({
      title: yup.string().required(),
      body: yup.number().required(),
      hargabeli: yup.number().required(),
      hargajual: yup.number().required(),
      image: yup
        .mixed()
        .test("fileSize", "File terlalu besar", (value) => value.size <= 100000)
        .test("fileType", "FORMAT TIDAK DIDUKUNG!", (value) =>
          SUPPORTED_FORMATS.includes(
            //  console.log( value.type )
            value.type
            //  === 'image/jpeg' || 'image/png' || 'image/jpg'
            // === 'image/png' || 'image/jpg'
          )
        ),
    });
    data
      .validate({
        title: content.title,
        body: content.body,
        image: image,
        hargabeli: content.hargabeli,
        hargajual: content.hargajual,
      })
      .then(function (valid) {
        if (valid) {
          setcoba(valid);
          console.log(valid);
          const data = new FormData();
          data.append("title", valid.title);
          data.append("body", valid.body);
          data.append("hargabeli", valid.hargabeli);
          data.append("hargajual", valid.hargajual);
          data.append("image", valid.image);

          axios.post('http://localhost:4000/v1/blog/post',data, {
            headers: {
              'conten-type' : 'multipart/form-data'
          }
          }).then(res=>{
            console.log('post sukses', res)
            setcontent({
              conditional:"Registrasi Berhasil"
            })
          })
        }
      })
      .catch((err) => console.log(err));
  };

  const onImageUpload = (e) => {
    const file = e.target.files[0];
    setImage(file);
  };
  console.log(content);

  return (
    <div className="body mt-4">
      <div className="container">
        <div className="row">
          <div className="card text-center">
            <p>{content.conditional} </p>
            <div className="card-header text-dark">Form Register</div>
            <div className="card-body">
              <h4 className="text-dark">{/* {register.conditional} */}</h4>

              <form className="container" onSubmit={handleSubmit}>
                <div className="form-group text-dark text-left ">
                  <label htmlFor="title">Nama Barang</label>
                  <input
                    name="title"
                    type="text"
                    className="form-control"
                    id="title"
                    placeholder="Nama Barang"
                    //   value={register.fullname|| ''}
                    onChange={handleChange}
                  />

                  <label htmlFor="hargabeli">Harga Beli</label>
                  <input
                    name="hargabeli"
                    type="number"
                    pattern="[0-9]*"
                    className="form-control"
                    id="hargabeli"
                    placeholder="Harga beli"
                    //   value={register.username|| ''}
                    onChange={handleChange}
                  />

                  <label htmlFor="email">Harga Jual</label>
                  <input
                    name="hargajual"
                    type="number"
                    className="form-control"
                    id="hargajual"
                    placeholder="harga Jual"
                    //   value={register.email|| ''}
                    onChange={handleChange}
                  />

                  <label htmlFor="stok">Stok</label>
                  <input
                    name="body"
                    type="number"
                    className="form-control"
                    id="body"
                    placeholder="Password"
                    //   value={register.password|| ''}
                    onChange={handleChange}
                  />
                  <label htmlFor="image" className="form-label">
                    Upload Image
                  </label>
                  <input
                    name="image"
                    type="file"
                    className="form-control"
                    id="image"
                    placeholder="image"
                    //   value={register.confPassword|| ''}
                    onChange={(e) => onImageUpload(e)}
                  />
                </div>

                <button type="submit" className="btn btn-primary mt-2">
                  Register
                </button>
              </form>

              <a href="/" type="submit" className="btn btn-primary mt-4">
                Back To Homepage
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Index;
