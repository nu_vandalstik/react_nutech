import React from "react";
import "./css/styles.css";
import "./js/scripts";


function index(props) {

    const {Konten, Footer, Sidebar} = props
  return (
    <div className="sb-nav-fixed">
      <link
        href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css"
        rel="stylesheet"
      />
      <script
        src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"
        crossOrigin="anonymous"
      ></script>
      <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        crossOrigin="anonymous"
      ></script>

      <script
        src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"
        crossOrigin="anonymous"
      ></script>

      <script
        src="https://cdn.jsdelivr.net/npm/simple-datatables@latest"
        crossOrigin="anonymous"
      ></script>

      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand ps-3" href="index.html">
          Halaman Admin
        </a>

        <button style={{color:"white"}}
          className="btn btn-sm order-1 order-lg-0 me-4 me-lg-0 btn-primary" 
          id="sidebarToggle"
          href="#!"
        >
            Menu
          <i className="fas fa-bars"></i>
        </button>
        <form className="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
          <img
            src="https://awsimages.detik.net.id/community/media/visual/2020/10/29/mark-zuckerberg-1_169.jpeg?w=700&q=90"
            style={{ width: 45, height: 45, borderRadius: 40 }}
          />
        </form>

        <ul className="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
          <li className="nav-item dropdown">
            <a
              className="nav-link dropdown-toggle"
              id="navbarDropdown"
              href="#"
              role="button"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <i className="fas fa-user fa-fw"></i>
            </a>
            <ul
              className="dropdown-menu dropdown-menu-end"
              aria-labelledby="navbarDropdown"
            >
              <li>
                <a className="dropdown-item" href="#!">
                  Settings
                </a>
              </li>
           
              <li>
                <hr className="dropdown-divider" />
              </li>
              <li>
                <a className="dropdown-item" href="#!">
                  Logout
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>

      {/*  */}

      <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
        {Sidebar}
        </div>
        <div id="layoutSidenav_content">
        

        {/* KONTEN */}

    {Konten}


        {/* end content */}
    
    {Footer}
        </div>
      </div>
    </div>
  );
}

export default index;
