import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios';

function Index() {
  const [data, setdata] = useState([])
  const url = window.location.pathname;
  const id = url.substring(url.lastIndexOf('/') + 1);
  const ret = id.replace(':', '');
  // console.log(ret);
  const api= ()=> {
 

    axios
    .get(
        `http://localhost:4000/v1/blog/post/${ret}`
    )
    .then((data) => {
        setdata([data.data.data]);
        // console.log(data.data.data);
    });
  }
  
  api()
  return (
      
    <div>    



    {data.map((data)=>{
      return(
<div className="card container mt-5" style={{width: '50rem'}} key={data._id}>
  <img src={'http://localhost:4000/'+data.image} className="card-img-top" alt="..."/>
  <div className="card-body">
    <h5 className="card-title">{data.title}</h5>
    <p className="card-text">Stok {data.body}</p>
    <p className="card-text">Harga Beli {data.hargabeli}</p>
    <p className="card-text">Harga Jual {data.hargajual}</p>
    
  </div>
  <div className="card-body">
    <Link to='' className="card-link">Card link</Link>
    <Link to='' className="card-link">Another link</Link>
  </div>

</div>
        
      ) 
    })}
  

</div>
    )
}

export default Index
