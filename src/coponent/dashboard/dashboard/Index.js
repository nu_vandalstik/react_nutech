import React, { useEffect, useState } from "react";
import axios from 'axios';
import { Link } from "react-router-dom";


function Index() {
const [content, setcontent] = useState([])
const [awal, setawal] = useState(1);
const [page, setpage] = useState({
    totalPages: 1,
    totalResult: 1
});
axios.get('http://localhost:4000/v1/blog/posts').then((data)=>{
  setpage({
    totalPages : data.data.total_data,
    totalResult : data.data.per_page

  })
})

const previous = () => (awal <= 1 ? 1 : setawal(awal - 1));

const next = () =>
    awal === page.totalPages ? page.totalPages : setawal(awal + 1);

const getData = () =>
axios
    .get(
        `http://localhost:4000/v1/blog/posts?page=${awal}&perPage=4`
    )
    .then((data) => {
      setcontent(data.data.data);
    })
    .catch((err) => err);

useEffect(() => {
  getData()
  // console.log(content)

})

// setTimeout(() => {
//   console.log(content)
// }, 5000);
  return (
    <div className="container mt-5">
    <div className="row align-items-start">

      {content.map((data)=>{
        return (
          <div className="col" key={data._id}>
        
          <div className="card" >
      <img src={'http://localhost:4000/'+data.image} style={{width:'25rem', height:'18rem'}}/>
      <div className="card-body">
        <h5 className="card-title">{data.title}</h5>
      </div>
      <ul className="list-group list-group-flush">
        <li className="list-group-item">Harga Beli {data.hargabeli}</li>
        <li className="list-group-item">Harga Jual {data.hargajual}</li>
        <li className="list-group-item">Stok {data.body}</li>
      </ul>
      <div className="card-body">
      <Link to={`/detail/:${data._id}`} className="btn btn-primary">Go somewhere</Link>
      </div>
    </div>

   
    </div>
        )
      })}
     

   



    </div>
    <nav aria-label="Page navigation example ">
                <ul className="pagination mt-4">
                    <li className="page-item">
                        <button
                            onClick={previous}
                            className="page-link"
                            aria-label="Previous"
                            type="submit"
                        >
                            <span aria-hidden="true">&laquo;</span>
                        </button>
                    </li>

                    <li className="page-item">
                        <p className="page-link">
                            {awal} / {page.totalPages}
                        </p>
                    </li>
                    <li className="page-item">
                        <button
                            className="page-link"
                            onClick={next}
                            type="submit"
                            aria-label="Next"
                        >
                            <span aria-hidden="true">&raquo;</span>
                        </button>
                    </li>
                </ul>
            </nav>
    </div>
  
  );
}

export default Index;
